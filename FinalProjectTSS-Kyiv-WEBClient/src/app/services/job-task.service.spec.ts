/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JobTaskService } from './job-task.service';

describe('Service: JobTask', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JobTaskService]
    });
  });

  it('should ...', inject([JobTaskService], (service: JobTaskService) => {
    expect(service).toBeTruthy();
  }));
});
