import { LoggedUser } from '../interfaces/logged-user';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

private host = 'https://localhost:44350/';
private currentUserSubject: BehaviorSubject<LoggedUser>;
public currentUser: Observable<LoggedUser>;
constructor(private http: HttpClient) {
  this.currentUserSubject = new BehaviorSubject<LoggedUser>(JSON.parse(localStorage.getItem('currentUser')));
  this.currentUser = this.currentUserSubject.asObservable();
}

public getCurrentUserValue(): LoggedUser{
  return this.currentUserSubject.value;
}

login(userLogin: any){
  return this.http.post(this.host + 'api/auth', userLogin).pipe(
    map((user: LoggedUser) => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }
    )
  );
}

logout(){
  localStorage.removeItem('currentUser');
  this.currentUserSubject.next(null);
}

register(userReg: any){
  return this.http.post(this.host + 'api/Auth/register', userReg);
}

}
