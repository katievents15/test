import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserProfile } from '../interfaces/user-profile';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  host = environment.apiUrl;

constructor(private http: HttpClient) { }

getUserById(id: number): Observable<UserProfile>{
  return this.http.get<UserProfile>(this.host + 'api/UserProfile/' + id);
}

updateUserById(id: number, user: any){
  return this.http.put(this.host + 'api/UserProfile/' + id, user);
}

}
