import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JobTask } from '../interfaces/job-task';
import { PageSetting } from '../interfaces/page-setting';

@Injectable({
  providedIn: 'root'
})
export class JobTaskService {
  host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getJobTask(): Observable<JobTask[]>{
    return this.http.get<JobTask[]>(this.host + 'api/JobTask');
  }

  getJobTaskByAssignee(assigneeLogin: string): Observable<PageSetting<JobTask[]>>{
    return this.http.get<PageSetting<JobTask[]>>(this.host + 'api/JobTask?' + "assignee=" + assigneeLogin);
  }

  getJobTaskByOwner(ownerLogin: string): Observable<PageSetting<JobTask[]>>{
    return this.http.get<PageSetting<JobTask[]>>(this.host + 'api/JobTask?' + "owner=" + ownerLogin);
  }

  getJobTaskByParams( params: any ): Observable<PageSetting<JobTask[]>>{
    return this.http.get<PageSetting<JobTask[]>>(this.host + 'api/JobTask' , { params });
  }

  getJobTaskId(id: any): Observable<JobTask>{
    return this.http.get<JobTask>(this.host + 'api/JobTask/' + id);
  }

  addJobTask(task: any){
    return this.http.post(this.host + 'api/JobTask', task);
  }

  updateJobTask(id: any, task: any){
    return this.http.put(this.host + 'api/JobTask/' + id, task);
  }
}
