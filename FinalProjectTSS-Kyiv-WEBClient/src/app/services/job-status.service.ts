import { JobStatus } from './../interfaces/job-status';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobStatusService {
  host = environment.apiUrl;
constructor(private http: HttpClient) { }

  getJobStatus(): Observable<JobStatus[]>{
    return this.http.get<JobStatus[]>(this.host + 'api/JobStatus');
  }

  addJobStatus(status: any){
    return this.http.post(this.host + 'api/JobStatus', status);
  }

  updateJobStatus(id: any, status: any){
    return this.http.put(this.host + 'api/JobStatus/' + id, status);
  }

  deleteJobStatus(id: string) {
    return this.http.delete(this.host + 'api/JobStatus/' + id);
  }

}
