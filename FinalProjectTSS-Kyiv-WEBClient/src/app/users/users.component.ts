import { Component, Injectable, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import {UserService} from './../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  constructor(private userServ: UserService, private router: Router ) {  }

  usersArray: User[] = [];
  user: User;
  errorUsers = '';
  userArrayForms: FormGroup[] = [];

  ngOnInit(): void {
    this.getUsersArray();
  }

  getUsersArray(): void {
    this.errorUsers = '';
    this.userServ.getUsers().subscribe((data) => {
      this.usersArray = data;
      console.log(data);
      this.createForms();
    },
    (ex) =>{
      console.log(ex);
      if(String(ex.status).substring(0,2) === '40'){
        this.errorUsers = 'Bad Request';
      }else if(String(ex.status).substring(0,2) === '50'){
        this.errorUsers = 'Internal Server Error';
      }else {
        this.errorUsers = 'Server unavailable';
      }

    }
    );
  }

  createForms(){
    let i = 0;
    for(let item of this.usersArray)
    {
      this.userArrayForms[i] = new FormGroup({
        id: new FormControl(item.id),
        login: new FormControl(item.login),
      });
      i++;
    }

  }

  viewForm(ngform: FormGroupDirective){
    //ngform.form.controls.editable.setValue(true);
    this.router.navigate(['/user-profile/' + ngform.form.value.id])
  }


}
