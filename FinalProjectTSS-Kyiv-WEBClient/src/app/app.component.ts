import { UsersComponent } from './users/users.component';
import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'FinalProjectTSS-Kyiv-WEBClient';
  public role: string;
  public login: string;
  profilId: number;
  isAuth = false;
  curUrl= '';

  constructor(private authService: AuthService,
              private router: Router ) {

    this.authService.currentUser.subscribe( user => {
      if( user){
        this.isAuth = true;
        this.login = user.login;
        this.profilId = user.userId;
      }
      else{
        this.isAuth = false;
        this.login = '';
        this.profilId = null;
      }
    })
  }

  ngOnInit(){
    let currentUser =  this.authService.getCurrentUserValue();
    this.isAuth = currentUser == null ? false : true;
    this.login = currentUser == null ? '' : currentUser.login;
    this.role = currentUser == null ? '' : currentUser.role;
    this.curUrl = this.router.url;
   }
  logout(){

    this.authService.logout();
    this.login = '';
    this.role = '';
    this.router.navigate(['/login']);
  }


  routerLinkReload(path: string){
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
   this.router.navigate([path]));
  }

}
