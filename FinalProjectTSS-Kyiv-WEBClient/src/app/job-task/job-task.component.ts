import { LoggedUser } from './../interfaces/logged-user';
import { AuthService } from './../services/auth.service';
import { UserService } from './../services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { JobTask } from '../interfaces/job-task';
import { JobTaskService } from '../services/job-task.service';
import { User } from '../interfaces/user';
import { IncludesInMassiveValidator } from '../helpers/custom-validator';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { HttpParams } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-job-task',
  templateUrl: './job-task.component.html',
  styleUrls: ['./job-task.component.scss']
})
export class JobTaskComponent implements OnInit {

  tasks: MatTableDataSource<JobTask>;
  task: JobTask;
  errorTask = '';
  viewTable = false;
  viewForm = false;
  taskForm: FormGroup;

  assignees: User[] = [];
  assignee: User[];
  curUser: LoggedUser;

  colForManager: string[] = ['title', 'description', 'startedDate', 'deadline', 'status', 'assignee', 'execution'];
  colForSpecialist: string[] = ['title', 'description', 'startedDate', 'deadline', 'status', 'owner', 'execution'];
  displayedColumns: string[] = ['title', 'description', 'startedDate', 'deadline', 'status', 'assignee', 'owner', 'execution'];

  // in
  defaultPageIndex: number = 0;
  defaultPageSize: number = 10;
  public defaultSortColumn: string = "title";
  public defaultSortOrder: string = "asc";

  defaultFilterColumn: string = "titleContains";
  filterQuery: string = null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // in


  constructor(private taskServ: JobTaskService, private userServ: UserService, private authServ: AuthService) { }

  ngOnInit() {

    this.curUser = this.authServ.getCurrentUserValue();
    this.viewTable = true;
    this.viewForm = false;
    if (this.curUser.role === 'Manager')
    {
      this.displayedColumns = this.colForManager;
    }
    else if(this.curUser.role === 'Specialist'){
      this.displayedColumns = this.colForSpecialist;
    }
    // this.getTasks();
    this.loadData(null); // in
  }


  // Pagination
  loadData(query: string = null) {
    var pageEvent = new PageEvent();
    pageEvent.pageIndex = this.defaultPageIndex;
    pageEvent.pageSize = this.defaultPageSize;
    // if (query) {
        this.filterQuery = query;
    // }
    this.getData(pageEvent);
  }

  getData(event: PageEvent) {

    // added after in
    this.defaultPageIndex = event.pageIndex ;
    this.defaultPageSize = event.pageSize;
    // added after in

    var params = new HttpParams()
      .set("pageIndex", event.pageIndex.toString())
      .set("pageSize", event.pageSize.toString())
      .set("sortColumn", (this.sort)
          ? this.sort.active
          : this.defaultSortColumn)
      .set("sortOrder", (this.sort)
          ? this.sort.direction
          : this.defaultSortOrder);

    if(this.curUser.role == "Specialist"){
      params = params.set("Assignee", this.curUser.login);
    }else if(this.curUser.role == "Manager"){
      params = params.set("Owner", this.curUser.login);
    }

    if (this.filterQuery) {
      params = params.set(this.defaultFilterColumn, this.filterQuery);
    }

    this.getTasks(params);
  }
  // Pagination

  getTasks(params: any){
      return this.taskServ.getJobTaskByParams(params).subscribe(
        (result) =>{
          console.log(result);
          this.tasks = new MatTableDataSource<JobTask>(result.data);
          this.paginator.length = result.totalCount;
          this.paginator.pageIndex = result.pageIndex;
          this.paginator.pageSize = result.pageSize;
        },
        (ex) =>{
          console.log(ex);
          if(String(ex.status).substring(0, 2) === '40'){
            this.errorTask = 'Bad Request';
          }else if(String(ex.status).substring(0, 2) === '50'){
            this.errorTask = 'Internal Server Error';
          }else {
            this.errorTask = 'Server unavailable';
          }
        }
      );

  }

  getAssignee(){
    return this.userServ.getAssignees().subscribe(
      (data) =>{
        console.log(data);
        this.assignees = data;
      },
      (ex) =>{
        console.log(ex);
        if(ex.status == 0)
        {
          this.errorTask = 'Server unavailable';
        }
        else{
          this.errorTask = ex.error;
        }
      }
    );
  }

  async ToggleTableForm(){
    if(!this.viewTable && this.viewForm){
      this.viewTable = true;
      this.viewForm = false;
      // this.getTasks()
      this.loadData(null);
    }else if(!this.viewForm && this.viewTable){
      this.viewTable = false;
      this.viewForm = true;
      await this.getAssignee();
      this.taskForm = new FormGroup({
        title: new FormControl(''),
        description: new FormControl('', Validators.required),
        deadline: new FormControl(''),
        status: new FormControl(''),
        owner: new FormControl(''),
        assignee: new FormControl('', ),
        remark: new FormControl(''),
        execution: new FormControl('')
      });
    }else {
      this.viewTable = true;
      this.viewForm = false;
    }

  }

  sendTask(){
    this.taskForm.value.owner = this.authServ.getCurrentUserValue().login;
    //this.taskForm.value.status = "Created";
    console.log(this.taskForm.value);
    this.taskServ.addJobTask(this.taskForm.value).subscribe(
      () => {
        // this.getTasks()
        this.loadData(null);
        this.ToggleTableForm();
      },
      (ex) => {
        console.log(ex);
        if(ex.status == 0)
        {
          this.errorTask = 'Server unavailable';
        }
        else{
          this.errorTask = ex.error;
        }
      }
    );

  }

  openEdit(curTask: JobTask){
    console.log(curTask);
  }

  updateValidAssignees(){
    this.taskForm.controls.assignee.setValidators(IncludesInMassiveValidator(this.assignees.map(x => x.login)));
  }
}
