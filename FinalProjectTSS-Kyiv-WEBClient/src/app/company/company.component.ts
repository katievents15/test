import { Company } from './../interfaces/company';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CompanyService } from '../services/company.service';
import { Router } from '@angular/router';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  company: Company;
  companyForm: FormGroup;
  createdForm = false;
  errorCompany = '';
  titleCompany = 'Company profile';
  color: ThemePalette = 'accent';

  constructor(private companyServ: CompanyService, private router: Router) { }

  ngOnInit() {
    this.getCompany();
  }

  getCompany(): void {
    this.errorCompany = '';
    this.companyServ.getCurrentCompany().subscribe((data) => {
      this.company = data;
      console.log(data);
      this.createForm();
  },
  (ex) =>{
    console.log(ex);
    if(ex.status == 0)
    {
      this.errorCompany = 'Server unavailable';
    }
    else{
      this.errorCompany = ex.error;
    }

  }
  );}

  createForm(){
    this.companyForm = new FormGroup({
      id: new FormControl(this.company.id),
      name: new FormControl(this.company.name),
      mail: new FormControl(this.company.mail),
      mailSubject: new FormControl(this.company.mailSubject),
      mailBody: new FormControl(this.company.mailBody),
      smtpHost: new FormControl(this.company.smtpHost),
      smtpPort: new FormControl(this.company.smtpPort),
      enableSsl: new FormControl(this.company.enableSsl),
      smtpLogin: new FormControl(this.company.smtpLogin),
      smtpPassword: new FormControl(this.company.smtpPassword),
    });
    this.createdForm = true;
  }

  update(){
    this.titleCompany = 'Company profile (saving)';
    this.companyServ.updateCompany(this.companyForm.value.id, this.companyForm.value).subscribe(
      () => {
        this.titleCompany = 'Company profile (successfully saved)';
        this.errorCompany = "";
      },
      (ex) => {
        console.log(ex);
        this.titleCompany = 'Company profile (save failed)';
        if(ex.status == 0)
        {
          this.errorCompany= 'Server unavailable';
        }
        else{
          this.errorCompany = ex.error;
        }
      }
    );
  }

  back(){
    this.router.navigate(['']);
  }

}
