import { JobStatus } from './../interfaces/job-status';
import { JobStatusService } from './../services/job-status.service';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

@Component({
  selector: 'app-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss']
})
export class JobStatusComponent implements OnInit {

  statusArray: JobStatus[] = [];
  statusArrayForms: FormGroup[] = [];
  status: JobStatus;
  errorStatus = '';



  constructor(private statusServ: JobStatusService) { }

  ngOnInit() {
    this.getStatusArray();
  }

  getStatusArray(): void {
    this.errorStatus = '';
    this.statusServ.getJobStatus().subscribe((data) => {
      this.statusArray = data;
      console.log(data);
      this.createForms();
  },
  (ex) =>{
    console.log(ex);
    if(ex.status == 0)
    {
      this.errorStatus = 'Server unavailable';
    }
    else{
      this.errorStatus = ex.error;
    }

  }
  );}

  createForms(){
    let i = 0;
    this.statusArrayForms = [];

    for(let item of this.statusArray)
    {
      this.statusArrayForms[i] = new FormGroup({
        id: new FormControl(item.id),
        name: new FormControl(item.name, Validators.required),
        order: new FormControl(item.order, Validators.required),
        editable: new FormControl(false),
        previousName: new FormControl(item.name)
      });
      i++;
    }

  }

  sendStatus(ngform: FormGroupDirective){
    console.log(ngform.form.value.id);
    if(ngform.form.value.id === '') // this.statusArray.map(x => x.id).includes('')
    {
      this.statusServ.addJobStatus(ngform.form.value).subscribe(
        () => {
          this.getStatusArray();
          this.createForms();
        },
        (ex) =>{
          console.log(ex);
          if(ex.status == 0)
          {
            this.errorStatus = 'Server unavailable';
          }
          else{
            this.errorStatus = ex.error;
          }

        }
      );
    }
    else{
      let id = ngform.form.value.id;
      this.statusServ.updateJobStatus(id, ngform.form.value).subscribe(
        () => {
          this.getStatusArray();
          this.createForms();
        },
        (ex) =>{
          console.log(ex);
          if(ex.status == 0)
          {
            this.errorStatus = 'Server unavailable';
          }
          else{
            this.errorStatus = ex.error;
          }

        }
      );
    }
  }

  removeStatus(id:string){
    if(id !== '')
    {
      this.statusServ.deleteJobStatus(id).subscribe(
        () => {
          this.getStatusArray();
          this.createForms();
        },
        (ex) =>{
          console.log(ex);
          if(ex.status == 0)
          {
            this.errorStatus = 'Server unavailable';
          }
          else{
            this.errorStatus = ex.error;
          }

        }
      );
    }
  }

  addRowStatus(){
    if(!this.statusArray.map(x => x.id).includes(''))
    {
      this.statusArray.push({id:"", name: ""});
      this.createForms();
    }
    let item = this.statusArrayForms.find(x => x.value.id == '') as FormGroup;

    item.controls.editable.setValue(true);
  }

  disabledAddRow():boolean{
    return this.statusArray.map(x => x.id).includes('');
  }

  editForm(ngform: FormGroupDirective){
    ngform.form.controls.editable.setValue(true);
  }


  cancelForm(ngform: FormGroupDirective){
    console.log(ngform.form.value.id);
    if(ngform.form.value.id !== '')
    {
      ngform.form.controls.editable.setValue(false);
      ngform.form.controls.name.setValue(ngform.form.value.previousName);
    }
    else{
      this.statusArray.splice(this.statusArray.findIndex(item => item.id === ""), 1)
      this.createForms();
    }
  }

}
