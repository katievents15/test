export interface LoggedUser {
  userId: number;
  login: string;
  role: string;
  token: string;
}
