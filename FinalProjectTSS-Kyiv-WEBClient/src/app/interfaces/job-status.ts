export interface JobStatus {
  id: string;
  name: string;
  order?: number;
}
