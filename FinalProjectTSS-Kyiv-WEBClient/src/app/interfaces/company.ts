export interface Company {
  id: number;
  name: string;
  mail: string;
  mailSubject: string;
  mailBody: string;
  smtpHost: string;
  smtpPort: number;
  enableSsl: boolean;
  smtpLogin: string;
  smtpPassword: string;
}
