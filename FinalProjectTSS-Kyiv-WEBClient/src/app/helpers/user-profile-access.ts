export class UserProfileAccess {
  userId = true;
  login = true;
  role = true;
  firstName = true;
  secondName = true;
  email = true;
  birthdayDay = true;
}
