import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';



export function IncludesInMassiveValidator(values: string[]): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} =>{
    const inputValue = control.value;

    if(!values.includes(inputValue)){
      return { "Value does't exist ": {inputValue}};
    }
    else{
      return null;
    }
  }
}




