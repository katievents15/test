export class JobTaskAccess {
  title = true;
  description = true;
  startedDate = true;
  deadline = true;
  remark = true;
  status = true;
  owner = true;
  assignee = true;
}
