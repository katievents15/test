import { AppComponent } from './../app.component';
import { Roles } from './../constants/Roles';
import { LoggedUser } from '../interfaces/logged-user';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Form, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginError: string;
  constructor(private authServ: AuthService, private router: Router, private app: AppComponent) {
    // redirect to home if already logged in
    if (this.authServ.getCurrentUserValue()) {
      this.router.navigate(['/']);
  }
  }

  ngOnInit(): void {
    this.loginError = '';
    this.loginForm = new FormGroup(
      {
        login: new FormControl(''),
        password: new FormControl('')
      }
    )
  }

  login(){
    this.authServ.login(this.loginForm.value).subscribe(
      (LoggedUser) =>
      {
        this.app.role = LoggedUser.role;
        this.app.login = LoggedUser.login;
        this.app.isAuth = true;
        if(LoggedUser.role === Roles.Manager){
          this.router.navigate(['/users']);
        }
        else{
          this.router.navigate(['']);
        }

      },
      (exc) => {
        this.loginError = exc.error;
      }
    )
  }

}
