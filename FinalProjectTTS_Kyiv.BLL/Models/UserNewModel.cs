﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class UserNewModel
    {
        public string Login { get; set; }

        public string Role { get; set; }
    }
}
