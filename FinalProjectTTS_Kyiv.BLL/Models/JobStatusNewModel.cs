﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class JobStatusNewModel
    {
        [Required]
        public string Name { get; set; }

        public uint Order { get; set; }
    }
}
