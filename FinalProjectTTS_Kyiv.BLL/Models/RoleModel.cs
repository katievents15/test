﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class RoleModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
