﻿using FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class CompanyModel: ISenderSetting, IMessageModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Mail { get; set; }

        public string MailSubject { get; set; }

        public string MailBody { get; set; }

        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public bool EnableSsl { get; set; }

        public string SmtpLogin { get; set; }

        public string SmtpPassword { get; set; }
    }
}
