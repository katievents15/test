﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Models
{
    public class UserProfileModel
    {
        public int UserId { get; set; }

        public string Login { get; set; }

        public string Role { get; set; }

        [RegularExpression(@"^[a-zA-Z][a-zA-Z\s`-]+$", ErrorMessage = "(ModelState) First Name must be only latin alphabet (can use - ` )")]
        public string FirstName { get; set; }

        [RegularExpression(@"^[a-zA-Z][a-zA-Z\s`-]+$", ErrorMessage = "(ModelState) Second Name must be only latin alphabet (can use - ` )")]
        public string SecondName { get; set; }

        public string Email { get; set; }

        public DateTime BirthdayDay { get; set; }
    }
}
