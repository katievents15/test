﻿using FinalProjectTTS_Kyiv.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Infrastructure.Filters;
using FinalProjectTTS_Kyiv.BLL.Infrastructure;

namespace FinalProjectTTS_Kyiv.BLL.Services.Interfaces
{
    public interface IJobTaskService: ICrud<JobTaskModel>
    {
        Task AddAsync(JobTaskNewModel model);

        Task<ApiResult<JobTaskModel>> GetAllWithFilter(JobTaskFilter filter, PageSetting pageSetting);
    }
}
