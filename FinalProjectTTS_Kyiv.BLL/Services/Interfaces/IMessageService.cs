﻿using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services.Interfaces
{
    public interface IMessageService
    {
        Task SendMessageAsync(JobTask task);
    }
}
