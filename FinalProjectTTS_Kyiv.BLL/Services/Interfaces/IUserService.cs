﻿using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services.Interfaces
{
    public interface IUserService : ICrud<UserModel>
    {
        Task AddAsync(UserNewModel model);

        Task<IEnumerable<UserModel>> GetAllByRole(string role);
    }
}
