﻿using AutoMapper;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Services
{
    public class RoleService: IRoleService
    {
        readonly IUnitOfWork uow;
        readonly IRoleRepository roleRepo;
        readonly IMapper mapper;

        public RoleService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.roleRepo = uow.RoleRepository;
            this.mapper = mapper;
        }

        public async Task AddAsync(RoleNewModel model)
        {
            var dao = mapper.Map<RoleNewModel, Role>(model);
            await roleRepo.CreateAsync(dao);
            await uow.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id, bool softDelete = true)
        {
            var daoContext = await roleRepo.GetByIdAsync(id);
            if (daoContext is null) { throw new Exception("Role doesn`t exist in DB"); }

            if (daoContext.Name == "Manager") { throw new Exception("Manager role can`t be deleted"); }

            await roleRepo.DeleteAsync(e => e.Id == id);
            await uow.SaveAsync();
        }

        public async Task<IEnumerable<RoleModel>> GetAll()
        {
            return mapper.Map<IEnumerable<Role>, IEnumerable<RoleModel>>(await roleRepo.GetAllAsync());
        }

        public async Task<RoleModel> GetByIdAsync(int id)
        {
            return mapper.Map<Role, RoleModel>(await roleRepo.GetByIdAsync(id));
        }

        public async Task UpdateAsync(RoleModel model)
        {
            var daoContext = await roleRepo.GetByIdAsync(model.Id);
            if (daoContext is null) { throw new Exception("Status doesn`t exist in DB"); }

            if (daoContext.Name == "Manager") { throw new Exception("Manager role can`t be renamed"); }

            var dao = mapper.Map<RoleModel, Role>(model, daoContext);
            await roleRepo.Update(dao);
            await uow.SaveAsync();
        }
    }
}
