﻿using AutoMapper;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace FinalProjectTTS_Kyiv.BLL.Services
{
    public class UserService : IUserService
    {
        readonly IUnitOfWork uow;
        readonly IUserRepository userRepo;
        readonly IMapper mapper;

        public UserService(IUnitOfWork uow, IMapper mapper)
        {
            this.uow = uow;
            this.userRepo = uow.UserRepository;
            this.mapper = mapper;
        }

        public async Task AddAsync(UserNewModel model)
        {
            var dao = mapper.Map<UserNewModel, User>(model);

            await userRepo.CreateAsync(dao);
            await uow.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id, bool softDelete = true)
        {
            var daoContext = await userRepo.GetByIdAsync(id);
            if (daoContext is null) { throw new Exception("User doesn`t exist in DB"); }

            await userRepo.DeleteAsync(e => e.Id == id);
            await uow.SaveAsync();
        }

        public async Task<IEnumerable<UserModel>> GetAll()
        {
            var daos = await userRepo.GetAllAsync();
            return mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(daos);
        }

        public async Task<IEnumerable<UserModel>> GetAllByRole(string role)
        {
            var daosContext = await userRepo.GetAllByAsync(x => x.Role.Name == role);
            return mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(daosContext);
        }

        public async Task<UserModel> GetByIdAsync(int id)
        {
            return mapper.Map<User, UserModel>(await userRepo.GetByIdAsync(id));
        }

        public async Task UpdateAsync(UserModel model)
        {
            var daoContext = await userRepo.GetByIdAsync(model.Id);
            if(daoContext is null) { throw new Exception("User doesn`t exist in DB"); }
            var dao = mapper.Map<UserModel, User>(model, daoContext);

            if(dao.Role.Name != model.Role)
            {
                var daoRole = await uow.RoleRepository.GetByAsync(x => x.Name == model.Role);
                if(daoRole is null) { throw new Exception("Role doesn`t exist in DB"); }

                dao.RoleId = daoRole.Id;
            }

            await userRepo.Update(dao);
            await uow.SaveAsync();
        }
    }
}
