﻿using FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService.Interfaces;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Implementations.Repositories;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService
{
    public class MessageServiceAdapter : IMessageService
    {
        ICompanyService companyServ;
        IUserProfileService userProServ;
        readonly OptionSMTP option;
        readonly Message message;

        public MessageServiceAdapter(ICompanyService companyServ, IUserProfileService userProServ)
        {
            this.companyServ = companyServ;
            this.userProServ = userProServ;
            this.option = new OptionSMTP();
            this.message = new Message();
        }

        public async Task SendMessageAsync(JobTask task)
        {
            var companySetting = await companyServ.GetCurrentCompany();
            
            GenerateOption(companySetting);
            var owner = await userProServ.GetByIdAsync(task.OwnerId);
            var assignee = await userProServ.GetByIdAsync(task.AssigneeId ?? 0);
            var listRecipient = new List<string>() { owner.Email, assignee.Email };
            CreateMessage(companySetting, task.Title, listRecipient);
            using (var mailSender = new SMTPSender())
            {
                mailSender.SendMessage(option, message);
            }
                
        }

        void GenerateOption(ISenderSetting companySetting)
        {
            option.Server = companySetting.SmtpHost;
            option.Port = companySetting.SmtpPort;
            option.Login = companySetting.SmtpLogin;
            option.Password = companySetting.SmtpPassword;
            option.EnableSsl = companySetting.EnableSsl;
        }

        void CreateMessage(IMessageModel companyMessage, string taskTitle, List<string> addressTo)
        {
            message.AddressFrom = companyMessage.Mail;
            message.Subject = companyMessage.MailSubject + $" (task - {taskTitle})";
            message.Body = companyMessage.MailBody;

            foreach(var address in addressTo)
            {
                message.AddressTo.Add(address);
            }
            
        }
    }
}
