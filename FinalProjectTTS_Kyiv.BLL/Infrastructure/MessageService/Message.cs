﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService
{
    public class Message
    {
        public string AddressFrom { get; set; }

        public List<string> AddressTo { get; set; } = new List<string>();

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
