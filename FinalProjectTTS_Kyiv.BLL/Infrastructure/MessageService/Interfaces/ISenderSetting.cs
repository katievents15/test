﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService.Interfaces
{
    public interface ISenderSetting
    {
        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        public bool EnableSsl { get; set; }

        public string SmtpLogin { get; set; }

        public string SmtpPassword { get; set; }
    }
}
