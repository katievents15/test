﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.MessageService.Interfaces
{
    public interface IMessageModel
    {
        public string Mail { get; set; }

        public string MailSubject { get; set; }

        public string MailBody { get; set; }
    }
}
