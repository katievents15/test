﻿using AutoMapper;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure.Helpers
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<CompanyModel, Company>().ReverseMap();

            CreateMap<UserNewModel, User>()
                .ForMember(dist => dist.Role, opt => opt.Ignore());
            CreateMap<User, UserModel>()
                .ForMember(dist => dist.Role, opt => opt.MapFrom(sour => sour.Role.Name));
            CreateMap<UserModel, User>()
                .ForMember(dist => dist.Role, opt => opt.Ignore());

            CreateMap<UserProfileModel, User>()
                .ForMember(dist => dist.Id, opt => opt.MapFrom(sour => sour.UserId))
                .ForMember(dist => dist.Role, opt => opt.Ignore());
            CreateMap<UserProfileModel, UserProfile>();
            CreateMap<User, UserProfileModel>()
                .ForMember(dist => dist.UserId, opt => opt.MapFrom(sour => sour.Id))
                .ForMember(dist => dist.Role, opt => opt.MapFrom(sour => sour.Role.Name))
                .ForMember(dist => dist.FirstName, opt => opt.MapFrom(sour => sour.UserProfile.FirstName))
                .ForMember(dist => dist.SecondName, opt => opt.MapFrom(sour => sour.UserProfile.SecondName))
                .ForMember(dist => dist.Email, opt => opt.MapFrom(sour => sour.UserProfile.Email))
                .ForMember(dist => dist.BirthdayDay, opt => opt.MapFrom(sour => sour.UserProfile.BirthdayDay));

            CreateMap<JobStatusModel, JobStatus>().ReverseMap();
            CreateMap<JobStatusNewModel, JobStatus>();

            CreateMap<RoleModel, Role>().ReverseMap();
            CreateMap<RoleNewModel, Role>();

            CreateMap<JobTask, JobTaskModel>()
                .ForMember(dist => dist.Status, opt => opt.MapFrom(sorc => sorc.Status.Name))
                .ForMember(dist => dist.Owner, opt => opt.MapFrom(sorc => sorc.Owner.Login))
                .ForMember(dist => dist.StatusOrder, opt => opt.MapFrom(sorc => sorc.Status.Order))
                .ForMember(dist => dist.Assignee, opt => opt.MapFrom(sorc => sorc.Assignee.Login));

            CreateMap<JobTaskModel, JobTask>()
                .ForMember(dist => dist.Status, opt => opt.Ignore())
                .ForMember(dist => dist.Owner, opt => opt.Ignore())
                .ForMember(dist => dist.Assignee, opt => opt.Ignore());

            CreateMap<JobTaskNewModel, JobTask>()
                .ForMember(dist => dist.Status, opt => opt.Ignore())
                .ForMember(dist => dist.Owner, opt => opt.Ignore())
                .ForMember(dist => dist.Assignee, opt => opt.Ignore());

        }
    }
}
