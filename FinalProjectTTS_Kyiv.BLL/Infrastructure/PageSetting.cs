﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace FinalProjectTTS_Kyiv.BLL.Infrastructure
{
    public class PageSetting
    {
        public int PageIndex { get; set; } = 0;
        public int PageSize { get; set; } = 10;
        public string SortColumn { get; set; }
        public TypeOfOrder? SortOrder { get; set; }

    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum TypeOfOrder
    {
        ASC, DESC
    }
}
