﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.DAL.Entities
{
    public class JobStatus: BaseEntity
    {
        public string Name { get; set; }

        public uint Order { get; set; }

        public virtual IList<JobTask> Tasks { get; set; }
    }
}
