﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.DAL.Entities
{
    public class UserProfile: BaseEntity
    {
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string Email { get; set; }

        public DateTime BirthdayDay { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
