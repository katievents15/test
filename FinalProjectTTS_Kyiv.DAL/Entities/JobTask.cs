﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.DAL.Entities
{
    public class JobTask : BaseEntity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime StartedDate { get; set; }

        public DateTime Deadline { get; set; }

        public string Remark { get; set; }

        public int StatusId { get; set; }
        public JobStatus Status { get; set; }

        public int OwnerId { get; set; }
        public User Owner { get; set; }

        public int? AssigneeId { get; set; }
        public User Assignee { get; set; }
    }
}
