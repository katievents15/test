﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProjectTTS_Kyiv.DAL.Entities
{
    public class Role: BaseEntity
    {
        public string Name { get; set; }

        public virtual IList<User> Users { get; set; }
    }
}
