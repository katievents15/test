﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FinalProjectTTS_Kyiv.DAL.Entities
{
    [Table("Companies")]
    public class Company: BaseEntity
    {
        public string Name { get; set; }

        public string Mail { get; set; }

        public string MailSubject { get; set; }

        public string MailBody { get; set; }

        public string SmtpHost { get; set; }

        public int? SmtpPort { get; set; }

        [DefaultValue(true)]
        public bool EnableSsl { get; set; }

        public string SmtpLogin { get; set; }

        public string SmtpPassword { get; set; }

    }
}
