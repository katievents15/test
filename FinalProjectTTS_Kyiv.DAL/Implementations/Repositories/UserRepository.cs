﻿using FinalProjectTTS_Kyiv.DAL.Data;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.DAL.Implementations.Repositories
{
    public class UserRepository : IUserRepository
    {
        readonly TTSAppContext context;
        readonly DbSet<User> curSet;

        public UserRepository(TTSAppContext context)
        {
            this.context = context;
            this.curSet = context.Users;
        }

        public async Task CreateAsync(User item)
        {
            await curSet.AddAsync(item);
        }

        public async Task DeleteAsync(Expression<Func<User, bool>> expression, bool softDelete = true)
        {
            var item = await curSet.FirstOrDefaultAsync(expression);          
            
            if (item != null)
            {
                if (softDelete)
                {
                    item.IsDeleted = true;
                    curSet.Update(item);
                }
                else
                    curSet.Remove(item);
            }
        }

        public async Task<List<User>> GetAllAsync()
        {
            return await curSet.Include(x => x.Role).Include(x => x.UserProfile).Where(x => !x.IsDeleted).ToListAsync();
        }

        public async Task<List<User>> GetAllByAsync(Expression<Func<User, bool>> expression)
        {
            return await curSet.Include(x => x.Role).Include(x => x.UserProfile).Where(x => !x.IsDeleted).Where(expression).ToListAsync();
        }

        public async Task<User> GetByAsync(Expression<Func<User, bool>> expression)
        {
            return await curSet.Include(x => x.Role).Include(x => x.UserProfile).Where(x => !x.IsDeleted).FirstOrDefaultAsync(expression);
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await curSet.Include(x => x.Role).Include(x => x.UserProfile).Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Update(User item)
        {
            var itemExist = await curSet.FirstOrDefaultAsync(x => x.Id == item.Id);

            if (itemExist != null)
            {
                if (!itemExist.IsDeleted)
                {
                    curSet.Update(item);
                }
            }
        }
    }
}
