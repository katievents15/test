﻿using FinalProjectTTS_Kyiv.DAL.Data;
using FinalProjectTTS_Kyiv.DAL.Entities;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.DAL.Implementations.Repositories
{
    public class CompanyRepository: ICompanyRepository
    {
        readonly TTSAppContext context;
        readonly DbSet<Company> curSet;

        public CompanyRepository(TTSAppContext context)
        {
            this.context = context;
            this.curSet = context.Companies;
        }

        public async Task CreateAsync(Company item)
        {
            await curSet.AddAsync(item);
        }

        public async Task DeleteAsync(Expression<Func<Company, bool>> expression, bool softDelete = true)
        {
            var item = await curSet.FirstOrDefaultAsync(expression);

            if (item != null)
            {
                if (softDelete)
                {
                    item.IsDeleted = true;
                    curSet.Update(item);
                }
                else
                    curSet.Remove(item);
            }
        }

        public async Task<List<Company>> GetAllAsync()
        {
            return await curSet.Where(x => !x.IsDeleted).ToListAsync();
        }

        public async Task<List<Company>> GetAllByAsync(Expression<Func<Company, bool>> expression)
        {
            return await curSet.Where(x => !x.IsDeleted).Where(expression).ToListAsync();
        }

        public async Task<Company> GetByAsync(Expression<Func<Company, bool>> expression)
        {
            return await curSet.Where(x => !x.IsDeleted).FirstOrDefaultAsync(expression);
        }

        public async Task<Company> GetByIdAsync(int id)
        {
            return await curSet.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task Update(Company item)
        {
            var itemExist = await curSet.FirstOrDefaultAsync(x => x.Id == item.Id);

            if (itemExist != null)
            {
                if (!itemExist.IsDeleted)
                {
                    curSet.Update(item);
                }
            }
        }
    }
}
