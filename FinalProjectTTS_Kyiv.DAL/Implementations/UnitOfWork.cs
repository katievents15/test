﻿using FinalProjectTTS_Kyiv.DAL.Data;
using FinalProjectTTS_Kyiv.DAL.Implementations.Repositories;
using FinalProjectTTS_Kyiv.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.DAL.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly TTSAppContext context;
        ICompanyRepository compRepo;
        IUserRepository userRepo;
        IUserProfileRepository userProRepo;
        IRoleRepository roleRepo;
        IJobStatusRepository jStatusRepo;
        IJobTaskRepository jTaskRepo;

        public UnitOfWork(TTSAppContext context)
        {
            this.context = context; 
        }

        public ICompanyRepository CompanyRepository
        {
            get
            {

                if (this.compRepo == null)
                {
                    this.compRepo = new CompanyRepository(context);
                }
                return compRepo;
            }
        }

        public IUserRepository UserRepository
        {
            get
            {

                if (this.userRepo == null)
                {
                    this.userRepo = new UserRepository(context);
                }
                return userRepo;
            }
        }

        public IUserProfileRepository UserProfileRepository
        {
            get
            {

                if (this.userProRepo == null)
                {
                    this.userProRepo = new UserProfileRepository(context);
                }
                return userProRepo;
            }
        }

        public IRoleRepository RoleRepository
        {
            get
            {

                if (this.roleRepo == null)
                {
                    this.roleRepo = new RoleRepository(context);
                }
                return roleRepo;
            }
        }

        public IJobStatusRepository JobStatusRepository
        {
            get
            {

                if (this.jStatusRepo == null)
                {
                    this.jStatusRepo = new JobStatusRepository(context);
                }
                return jStatusRepo;
            }
        }

        public IJobTaskRepository JobTaskRepository
        {
            get
            {

                if (this.jTaskRepo == null)
                {
                    this.jTaskRepo = new JobTaskRepository(context);
                }
                return jTaskRepo;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await context.SaveChangesAsync();
        }

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~UnitOfWork()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
