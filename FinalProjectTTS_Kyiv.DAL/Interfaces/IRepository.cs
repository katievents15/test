﻿using FinalProjectTTS_Kyiv.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(int id);

        Task<List<TEntity>> GetAllByAsync(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> GetByAsync(Expression<Func<TEntity, bool>> expression);

        Task CreateAsync(TEntity item);

        Task Update(TEntity item);

        Task DeleteAsync(Expression<Func<TEntity, bool>> expression, bool softDelete = true);

    }
}
