﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectTTS_Kyiv.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICompanyRepository CompanyRepository { get; }

        IUserRepository UserRepository { get; }

        IUserProfileRepository UserProfileRepository { get; }

        IRoleRepository RoleRepository { get; }

        IJobStatusRepository JobStatusRepository { get; }

        IJobTaskRepository JobTaskRepository { get; }

        Task<int> SaveAsync();
    }
}
