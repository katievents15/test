﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinalProjectTTS_Kyiv.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        readonly IRoleService roleService;

        public RoleController(IRoleService roleService)
        {
            this.roleService = roleService;
        }

        // GET: api/<RoleController>
        [HttpGet]
        public async Task<IEnumerable<RoleModel>> Get()
        {
            try
            {
                return await roleService.GetAll();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET api/<RoleController>/5
        [HttpGet("{id}")]
        public async Task<RoleModel> Get(int id)
        {
            try
            {
                return await roleService.GetByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<RoleController>
        [Authorize(Roles = "Manager")]
        [HttpPost]
        public async Task Post([FromBody] RoleNewModel model)
        {
            try
            {
                await roleService.AddAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<RoleController>/5
        [Authorize(Roles = "Manager")]
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] RoleModel model)
        {
            try
            {
                model.Id = id;
                await roleService.UpdateAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<RoleController>/5
        [Authorize(Roles = "Manager")]
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            try
            {
                await roleService.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
