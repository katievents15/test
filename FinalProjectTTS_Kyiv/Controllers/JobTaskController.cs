﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Infrastructure;
using FinalProjectTTS_Kyiv.BLL.Infrastructure.Filters;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinalProjectTTS_Kyiv.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobTaskController : ControllerBase
    {
        readonly IJobTaskService taskService;

        public JobTaskController(IJobTaskService taskService)
        {
            this.taskService = taskService;
        }

        // GET: api/<JobTaskController>
        [Authorize]
        [HttpGet]
        public async Task<ApiResult<JobTaskModel>> Get([FromQuery]  JobTaskFilter filter, [FromQuery] PageSetting pageSetting )
        {
            try
            {
                //if(Request.QueryString.HasValue)
                    return await taskService.GetAllWithFilter(filter, pageSetting);
                //else
                //    return await taskService.GetAll();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        // GET api/<JobTaskController>/5
        [Authorize]
        [HttpGet("{id}")]
        public async Task<JobTaskModel> Get(int id)
        {
            try
            {
                return await taskService.GetByIdAsync(id);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        // POST api/<JobTaskController>
        [Authorize(Roles = "Manager")]
        [HttpPost]
        public async Task Post([FromBody] JobTaskNewModel model)
        {
            try
            {
                await taskService.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        // PUT api/<JobTaskController>/5
        [Authorize]
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] JobTaskModel model)
        {
            try
            {
                model.id = id;
                await taskService.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        // DELETE api/<JobTaskController>/5
        [Authorize(Roles = "Manager")]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
