﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinalProjectTTS_Kyiv.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class JobStatusController : ControllerBase
    {
        readonly IJobStatusService statusService;

        public JobStatusController(IJobStatusService statusService)
        {
            this.statusService = statusService;
        }

        // GET: api/<JobStatusController>
        [HttpGet]
        public async Task<IEnumerable<JobStatusModel>> Get()
        {
            try
            {
                return await statusService.GetAll();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // GET api/<JobStatusController>/5
        [HttpGet("{id}")]
        public async Task<JobStatusModel> Get(int id)
        {
            try
            {
                return await statusService.GetByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // POST api/<JobStatusController>
        [Authorize(Roles = "Manager")]
        [HttpPost]
        public async Task Post([FromBody] JobStatusNewModel model)
        {
            try
            {
                await statusService.AddAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<JobStatusController>/5
        [Authorize(Roles = "Manager")]
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] JobStatusModel model)
        {
            try
            {
                model.Id = id;
                await statusService.UpdateAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<JobStatusController>/5
        [Authorize(Roles = "Manager")]
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            try
            {
                await statusService.DeleteByIdAsync(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
    }
}
