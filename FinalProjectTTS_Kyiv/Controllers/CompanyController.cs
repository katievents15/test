﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinalProjectTTS_Kyiv.BLL.Models;
using FinalProjectTTS_Kyiv.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinalProjectTTS_Kyiv.Controllers
{
    [Authorize(Roles = "Manager")]
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        readonly ICompanyService companyService;

        public CompanyController(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        // GET: api/<CompanyController>
        [HttpGet("singleCompany")]
        public async Task<CompanyModel> Get()
        {
            try
            {
                return await companyService.GetCurrentCompany();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<CompanyController>/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] CompanyModel model)
        {
            try
            {
                model.Id = id;
                await companyService.UpdateAsync(model);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

    }
}
